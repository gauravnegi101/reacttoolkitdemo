
export const NAVLINKS_LEFT_SECTION = [
    { label: 'Home', url: '/' },
    { label: 'About', url: '/about' },
    { label: 'Contect Us', url: '/contact_us' },
    { label: 'Products', url: '/products' }
];
