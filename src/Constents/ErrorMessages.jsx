
const ErrorMessages = {
    required: 'this field is required',
    email: 'Please enter valid email',
    phone: 'Please enter your phone number',
    invalidPhone: 'Please enter your valid phone number',
};

export default ErrorMessages;