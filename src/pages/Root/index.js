import React from 'react';
// import PropTypes from 'prop-types';
import { Outlet } from 'react-router-dom';
import Layout from '../../Components/Layout';

function Root() {
    return (
        <div className='main_root_div'>
            <Layout>
                <main>
                    <Outlet />
                </main>
            </Layout>
        </div>
    );
}

Root.propTypes = {};

export default Root;
