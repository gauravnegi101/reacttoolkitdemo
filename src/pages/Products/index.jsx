import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ProductsComponents from '../../Components/PageComponent/Products';
import { getAllProductApi, getColors } from '../../Components/Redux/Reducer/ProductSlice';
// import { useLoaderData } from 'react-router-dom';
// import PropTypes from 'prop-types'

function Products() {
    const { data: allProducts, error, isLoading, colors } = useSelector(state => state?.ProductData);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getAllProductApi());
        dispatch(getColors());
    }, []);

    console.log(allProducts, 'allProducts');
    if (isLoading) {
        return <>Loading ..........................</>;
    }
    if (error) {
        return <>{error}</>;
    }
    return (
        <ProductsComponents {...{ allProducts, colors }} />
    );
}

Products.propTypes = {};

export default Products;
