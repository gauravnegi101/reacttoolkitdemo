import React from 'react';
import HomeComponent from '../../Components/PageComponent/Home';
import withRouter from '../../Helper/withRouterHoC';
import MainHomeStyle from '../../Styles/Home.style';
// import PropTypes from 'prop-types';

const HomePage = (props) => {

    console.log(props);
    return (
        <MainHomeStyle className='mainHome_section'>
            <HomeComponent {...props} />
        </MainHomeStyle>
    );
};

HomePage.propTypes = {

};

export default withRouter(HomePage);
