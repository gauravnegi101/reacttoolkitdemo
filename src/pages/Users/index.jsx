import React from 'react';
import UsersComponent from '../../Components/PageComponent/Users';
import withRouter from '../../Helper/withRouterHoC';
import MainUsersStyle from '../../Styles/Users.style';
import PropTypes from 'prop-types';

function UsersPage(props) {
    return (
        <MainUsersStyle>
            <UsersComponent {...props} />
        </MainUsersStyle>
    );
}

UsersPage.propTypes = {
    props: PropTypes.object,
};

export default withRouter(UsersPage);
