import React from 'react';
import propTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

let activeStyle = {
    textDecoration: 'underline',
};

function NavLinkComp({ activeClassName, text, url }) {
    return (
        <NavLink
            to={url}
            className={({ isActive }) => isActive ? activeClassName : ''}
            style={({ isActive }) =>
                isActive ? activeStyle : undefined
            }
        >
            {text}
        </NavLink>
    );
}

NavLinkComp.propTypes = {
    activeClassName: propTypes.string,
    url: propTypes.string,
    text: propTypes.string
};
export default NavLinkComp;