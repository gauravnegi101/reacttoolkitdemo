import React, { useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import _ from 'lodash';

const SelectStyle = styled.div`
    select {
        padding: 10px 5px;
        background-color: #eee;
        border: 1px solid #ddd;
        color: #000;
        border-radius: 30px;
        font-size: 16px;
        &:focus-visible {
            outline:none;
        }
    }

`;

const SelectComponent = ({ options, fieldName, emptyText, classes, defaultValue, callBackFunction }) => {

    const [selectedValue, setSelectedValue] = useState(defaultValue || '');

    function _handleChange(value) {
        setSelectedValue(value);
        callBackFunction(value);
    }

    return (
        <SelectStyle className='select_component'>
            <select value={selectedValue} name={fieldName} className={`form-control ${classes}`} onChange={(e) => _handleChange(e.target.value)}>
                <option value="">{emptyText}</option>
                {_.map(options, ({ label, value }) => (
                    <option value={value}>{label}</option>
                ))}
            </select>
        </SelectStyle>
    );
};

SelectComponent.propTypes = {
    options: PropTypes?.array,
    fieldName: PropTypes?.string,
    classes: PropTypes?.string,
    defaultValue: PropTypes?.string,
    callBackFunction: PropTypes?.func,
    emptyText: PropTypes?.string
};
export default SelectComponent;