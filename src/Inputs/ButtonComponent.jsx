import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ButtonStyle = styled.div`
    button {
        padding: 12px 20px;
        border-radius: 30px;
        border: unset;
        font-size: 16px;
        cursor: pointer;
    }

`;

const ButtonComponent = ({ buttonTitle, onClick, classes, value }) => {
    return (
        <ButtonStyle className={`bU_${classes}`}>
            <button className={`button ${classes}`} value={value} onClick={() => onClick()}>{buttonTitle}</button>
        </ButtonStyle>
    );
};

ButtonComponent.propTypes = {
    buttonTitle: PropTypes.string,
    onClick: PropTypes.func,
    classes: PropTypes.string,
    value: PropTypes.string
};

export default ButtonComponent;