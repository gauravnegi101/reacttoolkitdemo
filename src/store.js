import { configureStore } from '@reduxjs/toolkit';
import logger from 'redux-logger';
import createDelete from './Components/Redux/Reducer/createDeleteUser';
import rootReducer from './Components/Redux/rootReducer';


const store = configureStore({
    reducer: rootReducer,
    middleware: (gDM) => gDM().concat([logger, createDelete.middleware])
});

export default store;