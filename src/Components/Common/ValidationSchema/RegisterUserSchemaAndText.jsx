
import * as yup from 'yup';


export const RegisterUserSchema = yup.object().shape({
    name: yup.string().trim().required('this field is required'),
    email: yup.string().trim()
        .required('email is required')
        .email('Please enter valid email')
});

export const RegisterTexts = {
    title: 'Register Form',
    updateTitle: 'Update User',
    name: 'Name',
    email: 'Email',
    phone: 'Phone Number',
    address: 'Address Line',
    city: 'City',
    zipcode: 'Zip Code',
    website: 'Website',
    username: 'User Name',
};