import React from 'react';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';

const PaginateComponent = ({ pageCount, handlePageClick }) => {
    return (
        <div className='pagination'>
            <ReactPaginate
                previousLabel={"<"}
                nextLabel={">"}
                breakLabel={"..."}
                breakClassName={"break-me"}
                pageCount={pageCount}
                marginPagesDisplayed={1}
                pageRangeDisplayed={5}
                onPageChange={handlePageClick}
                containerClassName={"pagination"}
                subContainerClassName={"pages_pagination"}
                activeClassName={"active"}
            />
        </div>
    )
}

PaginateComponent.propTypes = {
    pageCount: PropTypes?.number,
    handlePageClick: PropTypes?.func
}

export default index
