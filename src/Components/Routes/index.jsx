import { CircularProgress } from '@mui/material';
import React from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { Home, Products, Root, Users } from '../../pages';
import Error from '../Error';
import DisplayUser from '../PageComponent/Users/DisplayUser';
import RegisterUser from '../PageComponent/Users/RegisterUser';






const routers = createBrowserRouter([
    {
        path: '/',
        element: <Root />,
        errorElement: <Error />,
        children: [
            {
                path: '/',
                element: <Home />,
                // loader: teamLoader,
            },
            {
                path: 'about',
                element: <div>About</div>,
                // loader: teamLoader,
            },
            {
                path: 'contact_us',
                element: <div>Contect Us</div>,
                // loader: teamLoader,
            },
            {
                path: 'products',
                element: <Products />,
                // loader: () => api().root('http://localhost:4000/').get('users').s()
            },
            {
                path: 'users',
                element: <Users />,
                children: [
                    { path: ':id', element: <DisplayUser /> }
                ]
                // loader: () => api().root('http://localhost:4000/').get('users').s()
            },
            {
                path: 'register',
                element: <RegisterUser />
            }
        ],
    },
]);

function Routes() {
    return (
        <RouterProvider router={routers} fallbackElement={<CircularProgress />} />
    );
}
Routes.propTypes = {
    // children: PropTypes.node
};
export default Routes;
