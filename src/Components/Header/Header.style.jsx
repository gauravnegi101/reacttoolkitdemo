import styled from 'styled-components';

const HeaderStyle = styled.header`
    padding: 30px 0;
    background: linear-gradient(90deg, #0083ff 0%,#00ff9a 21%,#ff00f7 100%,transparent);
ul {
    display: flex;
    gap: 20px;
    margin: 0;
    list-style: none;

    li {
        a {
            font-size: 18px;
            font-weight: 600;
            text-decoration: none;
            &:hover {
                color: #9892ca;
            }
        }
    }
    
}

`;

export default HeaderStyle;