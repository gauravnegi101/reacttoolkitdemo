import React from 'react';
import _ from 'lodash';
import { NAVLINKS_LEFT_SECTION } from '../../Constents/HeaderConstents';
import NavLinkComp from '../../Inputs/NavlinkComp';
import HeaderStyle from './Header.style';

function Header() {
    return (
        <HeaderStyle className='main_header_section'>
            <nav>
                <ul>
                    {_.map(NAVLINKS_LEFT_SECTION, ({ label, url }, index) => (
                        <li key={index + 1}>
                            <NavLinkComp text={label} url={url} activeClassName='active' />
                        </li>
                    ))}
                </ul>
            </nav>
        </HeaderStyle>
    );
}

export default Header;