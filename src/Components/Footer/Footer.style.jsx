import styled from 'styled-components';

const FooterStyle = styled.footer`
    /* position: absolute;
    bottom: 0; */
    height: 200px;
    width: 100%;
    background: linear-gradient(90deg, #0083ff 0%,#00ff9a 21%,#ff00f7 100%,transparent);

    h2 {
        margin: 0;
    }
`;

export default FooterStyle;