import React from 'react';
import FooterStyle from './Footer.style';

function Footer() {
    return (
        <FooterStyle className='mainFooter_section'>
            <h2>This is footer</h2>
        </FooterStyle>
    );
}

export default Footer;