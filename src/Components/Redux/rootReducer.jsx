import { combineReducers } from '@reduxjs/toolkit';
import createDelete from './Reducer/createDeleteUser';
import ProductData from './Reducer/ProductSlice';

const rootReducer = combineReducers({
    [createDelete.reducerPath]: createDelete.reducer,
    ProductData,
});

export default rootReducer;