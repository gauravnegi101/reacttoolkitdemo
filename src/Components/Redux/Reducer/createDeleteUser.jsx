import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const createDelete = createApi({
    reducerPath: 'createDeleteApi',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:4000/',
    }),
    tagTypes: ['userData'],
    endpoints: (builder) => ({
        getAllUser: builder.query({
            query: () => 'users',
            providesTags: ['userData']
        }),
        getUserById: builder.query({
            query: (userId) => `users/${userId}`
        }),
        insertUserData: builder.mutation({
            query: (data) => ({
                url: 'users',
                method: 'POST',
                body: data,
            }),
            invalidatesTags: ['userData']
        }),
        deleteUser: builder.query({
            query: id => ({
                url: `/users/${id}`,
                method: 'DELETE'
            })
        }),
        userUpdate: builder.query({
            query: ({ id, ...data }) => ({
                url: `/users/${id}`,
                method: 'PUT',
                body: data
            })
        })
    }),
});

export const { useGetAllUserQuery, useGetUserByIdQuery, useInsertUserDataMutation, useUserUpdateQuery, useDeleteUserQuery } = createDelete;
export default createDelete;