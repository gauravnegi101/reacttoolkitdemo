import { createSlice } from '@reduxjs/toolkit';
import api from '../../../Helper/axiosHalper';

const ProductData = createSlice({
    name: 'products',
    initialState: {
        data: [],
        error: '',
        isLoading: false,
        colors: [],
    },
    reducers: {
        setAllProducts: (state, action) => {
            state['data'] = Array.isArray(action?.payload) ? action?.payload : [action?.payload];
        },
        setColors: (state, action) => {
            state['colors'] = action?.payload;
        },
        setErrorApi: (state, action) => {
            state['error'] = action?.payload;
        },
        setIsLoading: (state, action) => {
            state['isLoading'] = action.payload;
        }
    }
});
console.log(ProductData, 'Product_Data');
export const { setAllProducts, setErrorApi, setColors, setIsLoading } = ProductData.actions;

export const getAllProductApi = (id = null) => dispatch => {
    dispatch(setIsLoading(true));
    const URL_PATH = id ? `products/${id}` : 'products';
    api().root('http://localhost:4000/').get(URL_PATH).success((res) => {
        dispatch(setAllProducts(res));
        dispatch(setIsLoading(false));
    }).error((err) => {
        dispatch(setErrorApi(err));
        dispatch(setIsLoading(false));
    }).send();
};

export const getColors = () => dispatch => {
    dispatch(setIsLoading(true));
    api().root('http://localhost:4000/').get('colors').success((res) => {
        dispatch(setColors(res));
        dispatch(setIsLoading(false));
    }).error((err) => {
        dispatch(setErrorApi(err));
        dispatch(setIsLoading(false));
    }).send();
};
export default ProductData?.reducer;