import React from 'react';
import Footer from '../Footer';
import Header from '../Header';
import PropTypes from 'prop-types';

function Layout({ children }) {
    return (
        <div className='layout_css'>
            <Header />
            {children}
            <Footer />
        </div>
    );
}
Layout.propTypes = {
    children: PropTypes.node
};
export default Layout;