import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ButtonComponent from '../../../Inputs/ButtonComponent';
import DisplayAllUserStyle from './styles/DisplayAllUser.style';

const DisplayAllUsers = ({ userData, navigate }) => {

    return (
        <DisplayAllUserStyle>
            {_.map(userData, ({ id, name, username, email, phone, address }) => (
                <div className='userDetails' key={id}>
                    <div className="userDetailsInner">
                        <div className='image_section'>
                            <img src={`https://i.pravatar.cc/150?img=${id}`} alt='profileImage' />
                        </div>
                        <div className='details_section'>
                            <table>
                                <tbody>
                                    <tr><td>Name:</td><td>{name}</td></tr>
                                    <tr><td>Username:</td><td>{username}</td></tr>
                                    <tr><td>Email:</td><td>{email}</td></tr>
                                    <tr><td>Phone:</td><td>{phone}</td></tr>
                                    <tr><td>city:</td><td>{address?.city}</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <ButtonComponent value='' buttonTitle="Show Details" onClick={() => navigate(`${id}`)} classes='display_button' />
                </div>
            ))}
        </DisplayAllUserStyle>
    );
};

DisplayAllUsers.propTypes = {
    userData: PropTypes.array,
    navigate: PropTypes?.func
};

export default DisplayAllUsers;
