import { ErrorMessage, Field, Formik, Form } from 'formik';
import React from 'react';
import { RegisterUserSchema, RegisterTexts } from '../../Common/ValidationSchema/RegisterUserSchemaAndText';
import RegisterUserStyle from './styles/RegisterUser.style';
// import PropTypes from 'prop-types';

const { title, name, website, username, phone, email, address, city, zipcode } = RegisterTexts;

const FieldContainer = ({ section, label, name }) => (
    <div className={`${section}_Container`}>
        <div className='field_one'>
            <label htmlFor=''>{label}</label>
            <Field name={name} type='text' />
        </div>
        <ErrorMessage component="p" className='error' name={name} />
    </div>
);

const RegisterUser = () => {
    return (
        <Formik
            enableReinitialize
            initialValues={{
                name: 'Gaurav',
                username: '',
                email: '',
            }}
            validationSchema={RegisterUserSchema}

            onSubmit={values => {
                // same shape as initial values
                console.log(values);
            }}
        >
            <RegisterUserStyle>
                <Form className='main_user_register_section'>
                    <h1 className='title'>{title}</h1>
                    <div className='field_section'>
                        <FieldContainer label={name} name="name" section="first" />
                        <FieldContainer label={username} name="username" section="second" />
                    </div>
                    <div className='field_section'>
                        <FieldContainer label={email} name="email" section="first" />
                        <FieldContainer label={address} name="address" section="second" />
                    </div>
                    <div className='field_section'>
                        <FieldContainer label={city} name="city" section="first" />
                        <FieldContainer label={zipcode} name="zipcode" section="second" />
                    </div>
                    <div className='field_section'>
                        <FieldContainer label={phone} name="phone" section="first" />
                        <FieldContainer label={website} name="website" section="second" />
                    </div>
                    <div className='action_section'>
                        <button type='submit'>Submit</button>
                    </div>
                </Form>
            </RegisterUserStyle>
        </Formik>
    );
};

RegisterUser.propTypes = {};

export default RegisterUser;
