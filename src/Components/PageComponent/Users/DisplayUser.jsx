import React from 'react';
import { useOutletContext } from 'react-router-dom';
import { useGetUserByIdQuery } from '../../Redux/Reducer/createDeleteUser';
import DisplatUserStyle from './styles/DisplayUser.style';
// import PropTypes from 'prop-types';

const DisplayUser = () => {
    const value = useOutletContext();
    const { data: singleUserData, isLoading, error, isError } = useGetUserByIdQuery(value);

    if (isLoading) {
        return <>Loading....................................</>;
    }

    if (isError) {
        return <>Somthing went Wrong {error}</>;
    }

    return (
        <DisplatUserStyle className='single_user_details' imageUrl={`https://i.pravatar.cc/150?img=${value}`}>
            <div className='data_section'>
                <div className='title'>Image</div>
                <div className='user_data image_part'></div>
            </div>
            <div className='data_section'>
                <div className='title'>Name</div>
                <div className='user_data'>{singleUserData?.name}</div>
            </div>
            <div className='data_section'>
                <div className='title'>Username</div>
                <div className='user_data'>{singleUserData?.username}</div>
            </div>
            <div className='data_section'>
                <div className='title'>Email</div>
                <div className='user_data'>{singleUserData?.email}</div>
            </div>
            <div className='data_section'>
                <div className='title'>Address</div>
                <div className='user_data'>
                    {`${singleUserData?.address?.suite},${singleUserData?.address?.street},${singleUserData?.address?.city},${singleUserData?.address?.zipcode}`}
                </div>
            </div>
            <div className='data_section'>
                <div className='title'>Phone</div>
                <div className='user_data'>{singleUserData?.phone}</div>
            </div>
            <div className='data_section'>
                <div className='title'>Website</div>
                <div className='user_data'>{singleUserData?.website}</div>
            </div>
        </DisplatUserStyle>
    );
};

DisplayUser.propTypes = {};

export default DisplayUser;
