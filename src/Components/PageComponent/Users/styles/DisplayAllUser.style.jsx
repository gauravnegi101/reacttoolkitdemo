import styled from 'styled-components';

const DisplayAllUserStyle = styled.div`

    width: 100%;
    display: flex;
    flex-wrap: wrap;
    gap: 20px;

    .userDetails {
        box-shadow: 0px 0px 4px 2px #8f1515;
        max-width: calc(100%/3 - 50px);
        gap: 10px;
        flex: 100%;
        width: 100%;
        height: 100%;
        border: 1px solid gray;
        justify-content: space-around;
        padding: 10px;
        background: linear-gradient(45deg, #bf0808, transparent);
        border-radius: 10px;
        display: flex;
        flex-direction: column;

        .userDetailsInner {
            display: flex;
            gap: 20px;
            .image_section {
                height: 100%;
                width: 100%;

                img {
                    width: 100%;
                }
            }
            .details_section {
                table {
                    height: 100%;
                    border-collapse: collapse;
                    width: 100%;
                    min-width: 300px;
                    tr {
                        td {
                            padding: 5px 8px;
                            border: 1px solid #640b0b;
                            &:last-child {
                                text-align:center;
                            }
                        }
                    }
                }
            }
        }
        .display_button {
            width: 100%;
            background: #a11e1e;
            color: #fff;
        }
    }

`;

export default DisplayAllUserStyle;