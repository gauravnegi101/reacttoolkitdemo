import styled from 'styled-components';


const DisplatUserStyle = styled.div`

    display: flex;
    text-align: center;
    justify-content: space-evenly;
    width: 100%;
    max-width: 70%;
    margin: 0 auto 30px auto;
    box-shadow: 0px 0px 13px 3px #db1313;
    padding: 12px;
    border-radius: 30px;

    .data_section {
        .image_part {
            background-image:${props => props?.imageUrl ? `url(${props?.imageUrl})` : ''} ;
            background-repeat: no-repeat;
            background-size: cover;
            width: 140px;
            height: 140px;
            border-radius: 80px;
            background-position: center;
        }
        .title {
            padding: 10px;
            background: #000;
            color: #fff;
            margin-bottom: 10px;
        }
        .user_data {
            height: 140px;
            align-items: center;
            display: flex;
        }
    }

`;

export default DisplatUserStyle;