import styled from 'styled-components';


const RegisterUserStyle = styled.div`
    .main_user_register_section {
        height: 100%;
        width: 100%;
        margin: 0 auto;
        max-width: 800px;
        background: linear-gradient(360deg, #ab2424, transparent);
        padding: 30px;
        box-shadow: 0px 0px 14px 7px #a99304;
        border-radius: 10px;
        h1 {
            &.title {
                text-align: center;
                font-family: sans-serif;
                background: linear-gradient(45deg, #de1afb, transparent);
                /* padding: 10px; */
                -webkit-text-fill-color: transparent;
                -webkit-background-clip: text;

            }
        }

        .field_section {
            display: flex;
            margin-bottom: 20px;

            .first_Container,.second_Container {
                width: 100%;
                max-width: 50%;
                padding: 10px;
                border: 1px solid;
                margin-right: 10px;
            }

            .field_one,.field_second {
                display: flex;
                align-items: center;
                input[type="text"] {
                    padding: 12px 10px;
                    border: unset;
                    padding-left: 15px;
                    border-radius: 30px;
                    width: 100%;
                    font-size: 16px;
                    &:focus-visible {
                        outline: unset;
                    }
                }
                label {
                    width: 100%;
                    max-width: 100px;                
                }
            }
        }
        .action_section {
            text-align: center;
            border: 1px solid;
            padding: 20px;
            margin-right: 10px;
            button {
                width: 100%;
                cursor: pointer;
                max-width: 200px;
                padding: 12px;
                font-size: 20px;
                background: linear-gradient(360deg, #c7a8a8, transparent);
                border: 1px solid #eee;
                color: #fff;
            }
        }

    }

`;

export default RegisterUserStyle;