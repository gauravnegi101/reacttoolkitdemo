import ButtonComponent from '../../../Inputs/ButtonComponent';
import { useGetAllUserQuery } from '../../Redux/Reducer/createDeleteUser';
import PropTypes from 'prop-types';
import { Outlet } from 'react-router-dom';
import React from 'react';
import DisplayAllUsers from './DisplayAllUser';

const UsersComponent = ({ params, navigate }) => {

    const { data, isLoading } = useGetAllUserQuery();

    if (isLoading) {
        return <>Loading ................</>;
    }

    console.log(params, 'match>>>>>>>>>>>>>>>>>');
    return (
        <div className='main_Users_section'>
            <ButtonComponent buttonTitle='Back' onClick={() => navigate('/')} classes='back_button' value='' />

            <div className='single_user_data'>
                <Outlet context={params?.id} />
            </div>

            <DisplayAllUsers userData={data} navigate={navigate} />
        </div>
    );
};

UsersComponent.propTypes = {
    params: PropTypes.object,
    navigate: PropTypes?.func
};

export default UsersComponent;