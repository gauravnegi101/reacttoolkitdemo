import _ from 'lodash';
import PageComponentStyle from './Product.styled';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import SelectComponent from '../../../Inputs/SelectComponent';
import ButtonComponent from '../../../Inputs/ButtonComponent';

const ProductsComponents = ({ allProducts, colors }) => {
    const [colorSelect, setColorSelected] = useState([]);

    useEffect(() => {
        const ALL_DEFAULT_COLOR = _.map(allProducts, (row) => ({ id: row?.id, color: null }));
        setColorSelected(ALL_DEFAULT_COLOR);
    }, []);

    function _handleSelectColor(value) {
        alert(value);
    }

    function _handleOnClick(id) {
        alert(id);
    }

    return (
        <PageComponentStyle className='main_product_page'>
            {_.map(allProducts, (
                {
                    id,
                    product_image,
                    product_name,
                    color,
                    price,
                    discount_price,
                    description
                }
            ) => (
                <div className='product_container'>
                    <div className='image_section'>
                        <img src={product_image} />
                    </div>

                    <div className='all_details'>
                        <div className='name_section'>
                            <h3>{product_name}</h3>
                            <p>{description}</p>
                        </div>
                        <div className='colors'>
                            <SelectComponent
                                options={_.map(_.filter(colors, ({ id }) => _.includes(color, id)), ({ value, id }) => ({ label: value, value: id }))}
                                classes="color_classes"
                                fieldName="color"
                                emptyText="Select Color"
                                defaultValue={_.find(colorSelect, { id })?.color}
                                callBackFunction={(value) => _handleSelectColor(value)}
                            />
                        </div>
                        <div className='price_section'>
                            <div className='discount_price'>
                                <span>Price</span>: <span>{discount_price}</span>
                            </div>
                            <div className='mar_price'>
                                <span>MRP</span>: <span>{price}</span>
                            </div>
                        </div>
                        <ButtonComponent
                            buttonTitle="Add To Cart"
                            onClick={() => _handleOnClick(id)}
                            classes="add_to_cart"
                            value={id}
                        />
                    </div>
                </div>
            ))}
        </PageComponentStyle>
    );
};
ProductsComponents.propTypes = {
    allProducts: PropTypes.array,
    colors: PropTypes.array
};
export default ProductsComponents;
