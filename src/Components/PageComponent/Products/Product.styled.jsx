import styled from 'styled-components';

const PageComponentStyle = styled.div`
    display: flex;
    flex-wrap: wrap;
    gap: 30px;
    justify-content: center;
    height: 100%;
    overflow-y: scroll;

    .product_container {
        display: flex;
        border: 1px solid;
        max-width: 470px;
        box-sizing: border-box;
        width: 100%;
        box-shadow: 1px 1px 7px 2px #000;
        padding: 10px;
        justify-content: space-around;
        border-radius: 10px;
        text-align: center;
        background:linear-gradient(90deg, #c9c981 21%, #915959 72%);

        .image_section {
            width: 100%;
            max-width: 300px;
            display: flex;
            align-items: center;
            background: #fff;

            img {
                width: 100%;
                /* height: 100%; */
            }
        }

        .all_details {
            display: flex;
            color: #fff;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            gap: 20px;
            font-size: 20px;
            text-transform: capitalize;

            .name_section {
                h3 {
                    margin: 0;
                }
                p {
                    margin: 0;
                }
            }

            .colors {
                
            }
            .price_section {
                .mar_price {
                    text-decoration: line-through;
                }
            }
        }
    }

`;

export default PageComponentStyle;