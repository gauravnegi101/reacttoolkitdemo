import React from 'react';
import HomeButtonComponent from './HomeButtonComponent';

const HomeComponent = ({ navigate }) => {

    function _handleChange(value) {
        if (value === 'display') {
            navigate('/users');
        } else {
            navigate('/register');
        }
    }

    return (
        <div className='homePageComponet_css'>
            <HomeButtonComponent navigate={navigate} handleChange={(value) => _handleChange(value)} />
        </div>
    );
};

HomeComponent.propTypes = {};

export default HomeComponent;
