import styled from 'styled-components';


const HomeButtonComponentStyle = styled.div`
    height: 100%;
    margin-bottom: 20px;
    .buttons_section {
        display: flex;
        gap: 20px;
        align-items: center;
        flex-direction: row;
        justify-content: center;
        height: 700px;

        .register_user ,.display_user {
            button {
                font-size: 21px;
                padding: 60px 80px;
                font-size: 21px;
                background: linear-gradient(45deg,#00f 0%, #0ff 100%, transparent);
                color: #fff;
            }
        }
    }
`;

const HomeDisplayUserStyle = styled.div`
 

`;


export { HomeButtonComponentStyle, HomeDisplayUserStyle };