import React from 'react';
import ButtonComponent from '../../../Inputs/ButtonComponent';
import PropTypes from 'prop-types';
import { HomeButtonComponentStyle } from './HomeComponents.style';

const HomeButtonComponent = ({ handleChange }) => {
    return (
        <HomeButtonComponentStyle>
            <div className='buttons_section'>
                <div className='register_user'>
                    <ButtonComponent buttonTitle='Register' onClick={() => handleChange('register')} classes='' value='register' />
                </div>
                <div className='display_user'>
                    <ButtonComponent buttonTitle='Display' onClick={() => handleChange('display')} classes='' value="display" />
                </div>
            </div>
        </HomeButtonComponentStyle>
    );
};

HomeButtonComponent.propTypes = {
    handleChange: PropTypes.func,
};

export default HomeButtonComponent;
